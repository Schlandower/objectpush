if(!Object.prototype.push) {
	Object.prototype.push = function(rename, obj2, obj3 = {}, obj4 = {}, obj5 = {}) {
		var track = {};

		function combine(objx, objy, rename) {
			var newKey = '', tk = [], obj1keys = [], obj2keys = [], tn = 0;

			obj2keys = Object.keys(objy);
			if(obj2keys.length > 0) {
				obj1keys = Object.keys(objx);

				obj2keys.forEach((key) => {
					if(rename === true) {
						if(obj1keys.includes(key)) {
							tk = Object.keys(track);
							if(tk.includes(key)) {
								tn = (track[key] + 1);
								track[key] = tn;
								newKey = key + '____' + tn.toString(10);

								console.log('tn: ' + tn + '    tn.toString: ' + tn.toString(10) + '\nnewKey: ' + newKey);
							} else {
								newKey = key + '____0';
								track[key] = 0;
							}
						} else {
							newKey = key;
						}
					} else {
						newKey = key;
					}

					console.log('key: ' + key + '    newkey: ' + newKey);

					objx[newKey] = objy[key];
				});
			}
			return objx;
		}

		if(rename === undefined || rename === null || rename === '') {
			rename = false;
		}
		var outObj = {};
		var obj1 = this;
		if(typeof obj1 !== 'object' || obj1 === null || obj1 === undefined) {
			throw new TypeError('Type obj1: ' + typeof obj1 + ' is not an object!');
		}
		if(typeof obj2 !== 'object' || obj2 === null || obj2 === undefined) {
			throw new TypeError('Type obj2: ' + typeof obj2 + ' is not an object!');
		}
		if(Object.keys(obj3).length > 0 && (typeof obj3 !== 'object' || obj3 === null || obj3 === undefined)) {
			throw new TypeError('Type obj3: ' + typeof obj3 + ' is not an object!');
		}
		if(Object.keys(obj4).length > 0 && (typeof obj4 !== 'object' || obj4 === null || obj4 === undefined)) {
			throw new TypeError('Type obj4: ' + typeof obj4 + ' is not an object!');
		}
		if(Object.keys(obj5).length > 0 && (typeof obj5 !== 'object' || obj5 === null || obj5 === undefined)) {
			throw new TypeError('Type obj5: ' + typeof obj5 + ' is not an object!');
		}

		outObj = combine(obj1, obj2, rename);
		outObj = combine(outObj, obj3, rename);
		outObj = combine(outObj, obj4, rename);
		outObj = combine(outObj, obj5, rename);
		return outObj;
	};
}
